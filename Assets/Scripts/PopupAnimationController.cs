﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopupAnimationController : MonoBehaviour
{
    private Animator animator;
    [SerializeField]
    private GameObject panel = null;

    private void Start()
    {
        animator = GetComponent<Animator>();
    }

    public void Close()
    {
        animator.SetTrigger("CloseTrigger");
    }

    public void ClosePanel()
    {
        panel.SetActive(false);
    }
}
