﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class ButtonsController : MonoBehaviour
{
    [Header("Popup")]
    [SerializeField]
    private Button popupButton = null;
    [SerializeField]
    private GameObject popupPanel = null;

    [Header("Timed Text")]
    [SerializeField]
    private Button timedTextButton = null;
    [SerializeField]
    private GameObject timedTextPanel = null;
    [Min(1.0f)]
    [SerializeField]
    private float showTime = 3.0f;

    [Header("Sound")]
    [SerializeField]
    private Button soundButton = null;
    [SerializeField]
    private AudioManager audioManager = null;

    private List<int> activeSounds;

    void Start()
    {
        activeSounds = Enumerable.Range(0, audioManager.SoundsCount).ToList();
        popupButton.onClick.AddListener(OnPopupButtonClick);
        timedTextButton.onClick.AddListener(OnTimedTextButtonClick);
        soundButton.onClick.AddListener(OnSoundButtonClick);
    }
    public void OnPopupButtonClick()
    {
        popupPanel.SetActive(true);
    }
    private void OnTimedTextButtonClick()
    {
        StopCoroutine("ShowTimedText");
        StartCoroutine("ShowTimedText", showTime);
    }
    private void OnSoundButtonClick()
    {
        int r = Random.Range(0, activeSounds.Count);
        audioManager.PlaySound(activeSounds[r]);
    }
    public void OnSoundToggle(int num)
    {
        if (activeSounds.Contains(num))
        {
            activeSounds.Remove(num);
        } else
        {
            activeSounds.Add(num);
        }
        soundButton.interactable = activeSounds.Count > 0;
    }

    private IEnumerator ShowTimedText(float time)
    {
        timedTextPanel.SetActive(true);
        yield return new WaitForSeconds(time);
        timedTextPanel.SetActive(false);
    }
}
