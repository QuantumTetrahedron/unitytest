﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AudioManager : MonoBehaviour
{
    private AudioSource audioSource;

    [SerializeField]
    private List<AudioClip> sounds = null;

    public int SoundsCount { get { return sounds.Count; } }

    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    public void PlaySound(int num)
    {
        audioSource.clip = sounds[num];
        audioSource.Play();
    }
}
